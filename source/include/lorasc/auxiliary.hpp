////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/auxiliary.hpp
/// @brief   The auxiliary header
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_AUXILIARY_HPP_
#define LORASC_AUXILIARY_HPP_

#include "config.hpp"
#include "constant.hpp"
#include "auxiliary/schur.hpp"
#include "auxiliary/schur_impl.hpp"
#include "auxiliary/schur2conjugate.hpp"
#include "auxiliary/schur2conjugate_impl.hpp"
#include "auxiliary/lorasc_preconditioner.hpp"
#include "auxiliary/lorasc_preconditioner_impl.hpp"
#include "auxiliary/approximated_selfadjoint_eigen_solver_randomized_svd.hpp"
#include "auxiliary/approximated_selfadjoint_eigen_solver_randomized_svd_impl.hpp"

#endif  // LORASC_AUXILIARY_HPP_
