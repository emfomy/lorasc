////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/core/subdomain_impl.hpp
/// @brief   The implementation of subdomain
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_CORE_SUBDOMAIN_IMPL_HPP_
#define LORASC_CORE_SUBDOMAIN_IMPL_HPP_

#include "subdomain.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace LORASC
//
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default constructor
///
template <typename Scalar_, int uplo_>
Subdomain<Scalar_, uplo_>::Subdomain() {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default destructor
///
template <typename Scalar_, int uplo_>
Subdomain<Scalar_, uplo_>::~Subdomain() {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the size of this subdomain
///
/// @return  the size of this subdomain
///
template <typename Scalar_, int uplo_>
index_t Subdomain<Scalar_, uplo_>::size() const noexcept {
  return subdomain_.cols();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the subdomain matrix, A_ii
///
/// @return  a reference of the subdomain matrix, A_ii
///
//@{
template <typename Scalar_, int uplo_>
auto& Subdomain<Scalar_, uplo_>::subdomain() noexcept {
  return subdomain_;
}

template <typename Scalar_, int uplo_>
const auto& Subdomain<Scalar_, uplo_>::subdomain() const noexcept {
  return subdomain_;
}
//@}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the self-adjoint view of the subdomain matrix, A_ii
///
/// @return  a reference of the self-adjoint view of the subdomain matrix, A_ii
///
//@{
template <typename Scalar_, int uplo_>
auto Subdomain<Scalar_, uplo_>::subdomain_view() noexcept {
  return subdomain_.template selfadjointView<uplo_>();
}

template <typename Scalar_, int uplo_>
const auto Subdomain<Scalar_, uplo_>::subdomain_view() const noexcept {
  return subdomain_.template selfadjointView<uplo_>();
}
//@}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the interface matrix, A_is
///
/// @return  a reference of the interface matrix, A_is
///
//@{
template <typename Scalar_, int uplo_>
auto& Subdomain<Scalar_, uplo_>::interface() noexcept {
  return interface_;
}

template <typename Scalar_, int uplo_>
const auto& Subdomain<Scalar_, uplo_>::interface() const noexcept {
  return interface_;
}
//@}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the Cholesky solver for subdomain matrix
///
/// @return  a reference of the Cholesky solver for subdomain matrix
///
//@{
template <typename Scalar_, int uplo_>
auto& Subdomain<Scalar_, uplo_>::cholesky() noexcept {
  return cholesky_;
}

template <typename Scalar_, int uplo_>
const auto& Subdomain<Scalar_, uplo_>::cholesky() const noexcept {
  return cholesky_;
}
//@}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the subdomain temporary vectors
///
/// @return  a reference of the subdomain temporary vectors
///
//@{
template <typename Scalar_, int uplo_>
auto& Subdomain<Scalar_, uplo_>::vector0() noexcept {
  return vector0_;
}

template <typename Scalar_, int uplo_>
const auto& Subdomain<Scalar_, uplo_>::vector0() const noexcept {
  return vector0_;
}
//@}

}  // namespace lorasc

#endif  // LORASC_CORE_SUBDOMAIN_IMPL_HPP_
