////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/core/data.hpp
/// @brief   The definition of data
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_CORE_DATA_HPP_
#define LORASC_CORE_DATA_HPP_

#include <vector>
#include "../config.hpp"
#include "../constant.hpp"
#include "subdomain.hpp"
#include "separator.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace LORASC
//
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// The data structure
///
/// @tparam  Scalar_         the scalar type
/// @tparam  uplo_           the triangular storage, can be either @c Lower or @c Upper
/// @tparam  num_subdomain_  the number of subdomains
///
template <typename Scalar_, int uplo_, int num_subdomain_>
struct Data {

  /// The subdomains
  std::array<Subdomain<Scalar_, uplo_>, num_subdomain_> subdomain;

  /// The separator
  Separator<Scalar_, uplo_> separator;

};

}  // namespace lorasc

#endif  // LORASC_CORE_DATA_HPP_
