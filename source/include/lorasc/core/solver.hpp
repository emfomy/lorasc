////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/core/solver.hpp
/// @brief   The definition of LORASC solver
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_CORE_SOLVER_HPP_
#define LORASC_CORE_SOLVER_HPP_

#include "../config.hpp"
#include "../constant.hpp"
#include "data.hpp"
#include "../auxiliary/schur.hpp"
#include "../auxiliary/schur2conjugate.hpp"
#include "../auxiliary/lorasc_preconditioner.hpp"
#include "../auxiliary/approximated_selfadjoint_eigen_solver_randomized_svd.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace LORASC
//
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// The LORASC solver class
///
/// @tparam  Scalar_      the scalar type
/// @tparam  outer_uplo_  the triangular storage of input matrix, can be either @c Lower or @c Upper
/// @tparam  num_level_   the number of partition level
/// @tparam  uplo_        the inner triangular storage, can be either @c Lower or @c Upper
///
template <typename Scalar_, int outer_uplo_, int num_level_ = kNumLevel, int uplo_ = kUplo>
class Solver {

  static_assert(outer_uplo_ == Eigen::Lower || outer_uplo_ == Eigen::Upper,
                "lorasc::Solver only support outer_uplo_ = Eigen::Lower or Eigen::Upper.");
  static_assert(uplo_ == Eigen::Lower || uplo_ == Eigen::Upper,
                "lorasc::Solver only support uplo_ = Eigen::Lower or Eigen::Upper.");
  static_assert(num_level_ > 0, "lorasc::Solver require positive number of level.");

 protected:

  /// The number of subdomains
  static const int num_subdomain_ = 1 << num_level_;

 private:

  /// Type alias
  //@{
  typedef typename Eigen::NumTraits<Scalar_>::Real                                     RealScalar_;
  typedef Eigen::MatrixX<Scalar_>                                                      Matrix_;
  typedef Eigen::VectorX<Scalar_>                                                      Vector_;
  typedef Eigen::SpMatrixX<Scalar_>                                                    SpMatrix_;
  typedef Data<Scalar_, uplo_, num_subdomain_>                                         Data_;
  typedef Subdomain<Scalar_, uplo_>                                                    Subdomain_;
  typedef Separator<Scalar_, uplo_>                                                    Separator_;
  typedef Schur<Scalar_, uplo_, num_subdomain_>                                        Schur_;
  typedef Schur2Conjugate<Scalar_, uplo_, num_subdomain_>                              Schur2Conjugate_;
  typedef LorascPreconditioner<Scalar_, uplo_, num_subdomain_>                         Preconditioner_;
  typedef Eigen::ConjugateGradient<Schur_, Eigen::Lower|Eigen::Upper, Preconditioner_> IterativeSolver_;
  typedef ApproximatedSelfadjointEigenSolverRandomizedSVD<Schur2Conjugate_>            EigenSolver_;
  //@}

 protected:

  /// The maximum number of iterations used by iterative solver
  index_t max_iterations_;

  /// The tolerance threshold used by iterative solver
  RealScalar_ tolerance_;

  /// The rate of the rank used by randomized algorithm
  RealScalar_ rank_rate_;

  /// The rate of the trailing rank used by randomized algorithm
  RealScalar_ trail_rank_rate_;

  /// The data
  Data_ data_;

  /// The subdomains
  std::array<Subdomain_, num_subdomain_> &subdomain_ = data_.subdomain;

  /// The separator
  Separator_ &separator_ = data_.separator;

  /// The Schur complement
  Schur_ schur_;

  /// The conjugate of the second term of the Schur complement
  Schur2Conjugate_ schur2conjugate_;

  /// The conjugate gradient solver
  IterativeSolver_ iterative_solver_;

  /// The approximate eigenvalue solver
  EigenSolver_ eigen_solver_;

  /// The permutation matrix
  Eigen::PermutationMatrix<Eigen::Dynamic, Eigen::Dynamic, index_t> perm_;

  /// The inverse permutation matrix
  Eigen::PermutationMatrix<Eigen::Dynamic, Eigen::Dynamic, index_t> iperm_;

  /// The sizes of subdomains and separator
  std::vector<index_t> partition_size_;

  /// The starting indices of subdomains and separator
  std::vector<index_t> partition_start_;

 public:

  // Default constructor
  Solver();

  // Default destructor
  ~Solver();

  // Set maximum iterations
  auto& setMaxIterations( const index_t max_iterations ) noexcept;

  // Set tolerance
  auto& setTolerance( const RealScalar_ &tolerance ) noexcept;

  // Set rate of rank
  auto& setRankRate( const RealScalar_ rank_rate ) noexcept;

  // Set rate of trailing rank
  auto& setTrailRankRate( const RealScalar_ trail_rank_rate ) noexcept;

  // Computes decomposition
  auto& compute( const SpMatrix_ &matrix ) noexcept;

  // Solve linear system
  auto  solve( const Matrix_ &vector ) noexcept;

 protected:

  // Compute decomposition - Nested dissection
  void computeNestedDissection( const SpMatrix_ &matrix ) noexcept;

  // Compute decomposition - Matrix reordering
  void computeReordering( const SpMatrix_ &matrix ) noexcept;

  // Compute decomposition - Cholesky decomposition
  void computeCholesky() noexcept;

  // Compute decomposition - Creating Schur solvers
  void computeSchurSolver() noexcept;

  // Solve linear system - Forward substitution
  void solveForward() noexcept;

  // Solve linear system - Separator solving
  void solveSeparator() noexcept;

  // Solve linear system - Backward substitution
  void solveBackward() noexcept;

};

}  // namespace lorasc

#endif  // LORASC_CORE_SOLVER_HPP_
