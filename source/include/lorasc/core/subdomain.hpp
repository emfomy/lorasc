////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/core/subdomain.hpp
/// @brief   The definition of subdomain
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_CORE_SUBDOMAIN_HPP_
#define LORASC_CORE_SUBDOMAIN_HPP_

#include "../config.hpp"
#include "../constant.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace LORASC
//
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// The subdomain class
///
/// @tparam  Scalar_  the scalar type
/// @tparam  uplo_    the triangular storage, can be either @c Lower or @c Upper
///
template <typename Scalar_, int uplo_>
class Subdomain {

 private:

  /// Type alias
  //@{
  typedef Eigen::MatrixX<Scalar_>                        Matrix_;
  typedef Eigen::SpMatrixX<Scalar_>                      SpMatrix_;
  typedef Eigen::SparseSelfAdjointView<SpMatrix_, uplo_> View_;
  typedef Eigen::SimplicialLLT<SpMatrix_, uplo_>         Cholesky_;
  //@}

 protected:

  /// The subdomain matrix, A_ii
  SpMatrix_ subdomain_;

  /// The interface matrix, A_is
  SpMatrix_ interface_;

  /// The Cholesky solver for subdomain matrix
  Cholesky_ cholesky_;

  /// The subdomain right-hand-side vectors
  Matrix_ vector0_;

 public:

  // Default constructor
  Subdomain();

  // Default destructor
  ~Subdomain();

  // Get sizes
  index_t size() const noexcept;

  // Get references
  auto& subdomain() noexcept;
  const auto& subdomain() const noexcept;
  auto  subdomain_view() noexcept;
  const auto  subdomain_view() const noexcept;
  auto& interface() noexcept;
  const auto& interface() const noexcept;
  auto& cholesky() noexcept;
  const auto& cholesky() const noexcept;
  auto& vector0() noexcept;
  const auto& vector0() const noexcept;

};

}  // namespace lorasc

#endif  // LORASC_CORE_SUBDOMAIN_HPP_
