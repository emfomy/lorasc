////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/core/separator_impl.hpp
/// @brief   The implementation of separator
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_CORE_SEPARATOR_IMPL_HPP_
#define LORASC_CORE_SEPARATOR_IMPL_HPP_

#include "separator.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace LORASC
//
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default constructor
///
template <typename Scalar_, int uplo_>
Separator<Scalar_, uplo_>::Separator() {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default destructor
///
template <typename Scalar_, int uplo_>
Separator<Scalar_, uplo_>::~Separator() {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the size of this separator
///
/// @return  the size of this separator
///
template <typename Scalar_, int uplo_>
auto Separator<Scalar_, uplo_>::size() const noexcept {
  return separator_.cols();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the separator matrix, A_ss
///
/// @return  a reference of the separator matrix, A_ss
///
//@{
template <typename Scalar_, int uplo_>
auto& Separator<Scalar_, uplo_>::separator() noexcept {
  return separator_;
}

template <typename Scalar_, int uplo_>
const auto& Separator<Scalar_, uplo_>::separator() const noexcept {
  return separator_;
}
//@}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the self-adjoint view of the separator matrix, A_ss
///
/// @return  a reference of the self-adjoint view of the separator matrix, A_ss
///
//@{
template <typename Scalar_, int uplo_>
auto Separator<Scalar_, uplo_>::separator_view() noexcept {
  return separator_.template selfadjointView<uplo_>();
}

template <typename Scalar_, int uplo_>
const auto Separator<Scalar_, uplo_>::separator_view() const noexcept {
  return separator_.template selfadjointView<uplo_>();
}
//@}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the Cholesky solver for separator matrix
///
/// @return  a reference of the Cholesky solver for separator matrix
///
//@{
template <typename Scalar_, int uplo_>
auto& Separator<Scalar_, uplo_>::cholesky() noexcept {
  return cholesky_;
}

template <typename Scalar_, int uplo_>
const auto& Separator<Scalar_, uplo_>::cholesky() const noexcept {
  return cholesky_;
}
//@}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the eigenvectors of the Schur complement
///
/// @return  a reference of the eigenvectors of the Schur complement
///
//@{
template <typename Scalar_, int uplo_>
auto& Separator<Scalar_, uplo_>::eigenvectors() noexcept {
  return eigenvectors_;
}

template <typename Scalar_, int uplo_>
const auto& Separator<Scalar_, uplo_>::eigenvectors() const noexcept {
  return eigenvectors_;
}
//@}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the corrected eigenvalues of the Schur complement
///
/// @return  a reference of the corrected eigenvalues of the Schur complement
///
//@{
template <typename Scalar_, int uplo_>
auto& Separator<Scalar_, uplo_>::eigenvalues() noexcept {
  return eigenvalues_;
}

template <typename Scalar_, int uplo_>
const auto& Separator<Scalar_, uplo_>::eigenvalues() const noexcept {
  return eigenvalues_;
}
//@}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the separator temporary vectors
///
/// @return  a reference of the separator temporary vectors
///
//@{
template <typename Scalar_, int uplo_>
auto& Separator<Scalar_, uplo_>::vector0() noexcept {
  return vector0_;
}

template <typename Scalar_, int uplo_>
const auto& Separator<Scalar_, uplo_>::vector0() const noexcept {
  return vector0_;
}

template <typename Scalar_, int uplo_>
auto& Separator<Scalar_, uplo_>::vector1() noexcept {
  return vector1_;
}

template <typename Scalar_, int uplo_>
const auto& Separator<Scalar_, uplo_>::vector1() const noexcept {
  return vector1_;
}
//@}

}  // namespace lorasc

#endif  // LORASC_CORE_SEPARATOR_IMPL_HPP_
