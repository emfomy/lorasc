////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/core/separator.hpp
/// @brief   The definition of separator
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_CORE_SEPARATOR_HPP_
#define LORASC_CORE_SEPARATOR_HPP_

#include "../config.hpp"
#include "../constant.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace LORASC
//
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// The separator class
///
/// @tparam  Scalar_  the scalar type
/// @tparam  uplo_    the triangular storage, can be either @c Lower or @c Upper
///
template <typename Scalar_, int uplo_>
class Separator {

 private:

  /// Type alias
  //@{
  typedef Eigen::MatrixX<Scalar_>                        Matrix_;
  typedef Eigen::VectorX<Scalar_>                        Vector_;
  typedef Eigen::SpMatrixX<Scalar_>                      SpMatrix_;
  typedef Eigen::SparseSelfAdjointView<SpMatrix_, uplo_> View_;
  typedef Eigen::SimplicialLLT<SpMatrix_, uplo_>         Cholesky_;
  //@}

 protected:

  /// The separator matrix, A_ss
  SpMatrix_ separator_;

  /// The Cholesky solver for separator matrix
  Cholesky_ cholesky_;

  /// the eigenvectors of the Schur complement
  Matrix_ eigenvectors_;

  /// the corrected eigenvalues of the Schur complement
  Vector_ eigenvalues_;

  /// The separator right-hand-side vectors
  Matrix_ vector0_;
  Matrix_ vector1_;

 public:

  // Default constructor
  Separator();

  // Default destructor
  ~Separator();

  // Get sizes
  auto size() const noexcept;

  // Get references
  auto& separator() noexcept;
  const auto& separator() const noexcept;
  auto  separator_view() noexcept;
  const auto  separator_view() const noexcept;
  auto& cholesky() noexcept;
  const auto& cholesky() const noexcept;
  auto& eigenvectors() noexcept;
  const auto& eigenvectors() const noexcept;
  auto& eigenvalues() noexcept;
  const auto& eigenvalues() const noexcept;
  auto& vector0() noexcept;
  const auto& vector0() const noexcept;
  auto& vector1() noexcept;
  const auto& vector1() const noexcept;

};

}  // namespace lorasc

#endif  // LORASC_CORE_SEPARATOR_HPP_
