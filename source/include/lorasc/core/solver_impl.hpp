////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/core/solver_impl.hpp
/// @brief   The implementation of LORASC solver
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_CORE_SOLVER_IMPL_HPP_
#define LORASC_CORE_SOLVER_IMPL_HPP_

#include <metis.h>
#include "solver.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace LORASC
//
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default constructor
///
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
Solver<Scalar_, outer_uplo_, num_level_, uplo_>::Solver()
  : max_iterations_(kMaxIterations),
    tolerance_(kTolerance),
    rank_rate_(kRankRate),
    trail_rank_rate_(kTrailRankRate),
    schur_(data_),
    schur2conjugate_(data_) {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default destructor
///
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
Solver<Scalar_, outer_uplo_, num_level_, uplo_>::~Solver() {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the maximum number of iterations used by iterative solver
///
/// @param  max_iterations  the maximum number of iterations used by iterative solver
///
/// @return         this solver
///
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
auto& Solver<Scalar_, outer_uplo_, num_level_, uplo_>::setMaxIterations( const index_t max_iterations ) noexcept {
  max_iterations_ = max_iterations;
  return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Set tolerance threshold used by iterative solver
///
/// @param  tolerance  tolerance threshold used by iterative solver
///
/// @return         this solver
///
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
auto& Solver<Scalar_, outer_uplo_, num_level_, uplo_>::setTolerance( const RealScalar_ &tolerance ) noexcept {
  tolerance_ = tolerance;
  return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the rate of the rank used by randomized algorithm
///
/// @param  rank_rate  the rate of the rank used by randomized algorithm
///
/// @return         this solver
///
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
auto& Solver<Scalar_, outer_uplo_, num_level_, uplo_>::setRankRate( const RealScalar_ rank_rate ) noexcept {
  rank_rate_ = rank_rate;
  return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the rate of the trailing rank used by randomized algorithm
///
/// @param  trail_rank_rate  the rate of the trailing rank used by randomized algorithm
///
/// @return         this solver
///
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
auto& Solver<Scalar_, outer_uplo_, num_level_, uplo_>::setTrailRankRate( const RealScalar_ trail_rank_rate ) noexcept {
  trail_rank_rate_ = trail_rank_rate;
  return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Computes the LORASC decomposition
///
/// @param  matrix  the sparse positive definite matrix
///
/// @return         the decomposed solver
///
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
auto& Solver<Scalar_, outer_uplo_, num_level_, uplo_>::compute( const SpMatrix_ &matrix ) noexcept {

#ifdef LORASC_PROFILE
std::cout << std::left << std::setw(48) << "Nested dissection" << std::flush;
auto start = omp_get_wtime();
#endif

  computeNestedDissection(matrix);

#ifdef LORASC_PROFILE
std::cout << '\r' << std::left << std::setw(28) << "Nested dissection"
          << std::setw(12) << (omp_get_wtime() - start) << " sec" << std::endl;
#endif

#ifdef LORASC_PROFILE
std::cout << std::left << std::setw(48) << "Matrix reordering" << std::flush;
start = omp_get_wtime();
#endif

  computeReordering(matrix);

#ifdef LORASC_PROFILE
std::cout << '\r' << std::left << std::setw(28) << "Matrix reordering"
          << std::setw(12) << (omp_get_wtime() - start) << " sec" << std::endl;
#endif

  // Cholesky decomposition
#ifdef LORASC_PROFILE
std::cout << std::left << std::setw(48) << "Cholesky decomposition" << std::flush;
start = omp_get_wtime();
#endif

  computeCholesky();

#ifdef LORASC_PROFILE
std::cout << '\r' << std::left << std::setw(28) << "Cholesky decomposition"
          << std::setw(12) << (omp_get_wtime() - start) << " sec" << std::endl;
#endif

  // Creating Schur solvers
#ifdef LORASC_PROFILE
std::cout << std::left << std::setw(48) << "Creating Schur solvers" << std::flush;
start = omp_get_wtime();
#endif

  computeSchurSolver();

#ifdef LORASC_PROFILE
std::cout << '\r' << std::left << std::setw(28) << "Creating Schur solvers"
          << std::setw(12) << (omp_get_wtime() - start) << " sec" << std::endl;
#endif

  return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Computes decomposition - Nested dissection
///
/// @param  matrix  the sparse positive definite matrix
///
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
void Solver<Scalar_, outer_uplo_, num_level_, uplo_>::computeNestedDissection( const SpMatrix_ &matrix ) noexcept {

  const index_t matrix_size = matrix.cols();

  // Convert matrix to graph
  SpMatrix_ lower = matrix;
  if ( !lower.isCompressed() ) {
    lower.makeCompressed();
  }
  lower.prune([](const index_t &row, const index_t &col, const Scalar_&) { return (row != col); });
  SpMatrix_ graph = lower.template selfadjointView<outer_uplo_>();

  // Resize permutation matrices
  perm_.resize(matrix_size);
  iperm_.resize(matrix_size);

  // Resize vectors
  partition_size_.reserve(2*num_subdomain_-1);
  partition_start_.reserve(num_subdomain_+1);
  partition_size_.resize(num_subdomain_+1);
  partition_start_.resize(num_subdomain_+1);

  // METIS nested dissection
  auto info = METIS_NodeNDP(
      matrix_size,              // the number of vertices in the graph
      graph.outerIndexPtr(),    // the adjacency structure
      graph.innerIndexPtr(),    // the adjacency structure
      nullptr,                  // the weights of vertices
      num_subdomain_,           // the number of subdomains
      nullptr,                  // the options
      perm_.indices().data(),   // the permutation vector
      iperm_.indices().data(),  // the inverse permutation vector
      partition_size_.data()    // the size of subdomains
  );
  assert(info == METIS_OK);
  static_cast<void>(info);

  // Reorder inverse permutation vector and count starting indices of subdomains and separator
  auto perm_ptr  = perm_.indices().data();
  auto iperm_ptr = iperm_.indices().data();
  auto iperm_end = iperm_.indices().data() + matrix_size;
  for ( auto i = 0; i < num_subdomain_; ++i ) {
    std::copy(perm_ptr, perm_ptr + partition_size_[i], iperm_ptr);
    partition_start_[i] = iperm_ptr - iperm_.indices().data();
    perm_ptr  += partition_size_[i];
    iperm_ptr += partition_size_[i];
    for ( auto j = i; j % 2; ) {
      j = j / 2 + num_subdomain_;
      iperm_end -= partition_size_[j];
      std::copy(perm_ptr, perm_ptr + partition_size_[j], iperm_end);
      perm_ptr += partition_size_[j];
    }
  }
  partition_start_[num_subdomain_] = iperm_ptr - iperm_.indices().data();
  partition_size_[num_subdomain_] = matrix_size - partition_start_[num_subdomain_];

  // Compute permutation vector
  perm_ = iperm_.transpose();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Computes decomposition - matrix reordering
///
/// @param  matrix  the sparse positive definite matrix
///
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
void Solver<Scalar_, outer_uplo_, num_level_, uplo_>::computeReordering( const SpMatrix_ &matrix ) noexcept {

  // Reorder matrix
  SpMatrix_ matrix_ordered;
  matrix_ordered = matrix.template selfadjointView<outer_uplo_>().twistedBy(perm_);

  // Copy data to subdomains and separator
  for ( auto i = 0; i < num_subdomain_; ++i ) {
    subdomain_[i].subdomain() = matrix_ordered.template triangularView<uplo_>().block(
        partition_start_[i], partition_start_[i], partition_size_[i], partition_size_[i]
    );
    subdomain_[i].interface() = matrix_ordered.block(
        partition_start_[i], partition_start_[num_subdomain_], partition_size_[i], partition_size_[num_subdomain_]
    );
#ifdef LORASC_PROFILE
std::cout << '.' << std::flush;
#endif
  }
  separator_.separator() = matrix_ordered.template triangularView<uplo_>().bottomRightCorner(
      partition_size_[num_subdomain_], partition_size_[num_subdomain_]
  );
#ifdef LORASC_PROFILE
std::cout << '.' << std::flush;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Computes decomposition - Cholesky decomposition
///
/// @param  matrix  the sparse positive definite matrix
///
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
void Solver<Scalar_, outer_uplo_, num_level_, uplo_>::computeCholesky() noexcept {

  // Compute the Cholesky decomposition for subdomain matrices
  for ( auto i = 0; i < num_subdomain_; ++i ) {
    subdomain_[i].cholesky().compute(subdomain_[i].subdomain());
    assert(subdomain_[i].cholesky().info() == Eigen::Success);
#ifdef LORASC_PROFILE
std::cout << '.' << std::flush;
#endif
  }

  // Compute the Cholesky decomposition for separator matrix
  separator_.cholesky().compute(separator_.separator());
  assert(separator_.cholesky().info() == Eigen::Success);
#ifdef LORASC_PROFILE
std::cout << '.' << std::flush;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Computes decomposition - Creating Schur solvers
///
/// @param  matrix  the sparse positive definite matrix
///
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
void Solver<Scalar_, outer_uplo_, num_level_, uplo_>::computeSchurSolver() noexcept {

  eigen_solver_.setRankRate(rank_rate_).setTrailRankRate(trail_rank_rate_);
  eigen_solver_.compute(schur2conjugate_);

  // Correct the eigenvalues
  Scalar_ tmp = eigen_solver_.eigenvalues().tail(1).value();
  separator_.eigenvalues() = (1-tmp) * (1-eigen_solver_.eigenvalues().array()).inverse() - 1;
  separator_.eigenvectors() = separator_.cholesky().matrixU().solve(eigen_solver_.eigenvectors());

  // Create the iterative solver
  iterative_solver_.setMaxIterations(max_iterations_).setTolerance(tolerance_);
  iterative_solver_.preconditioner().data(data_);
  iterative_solver_.compute(schur_);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Solve the linear system using current LORASC decomposition
///
/// @param  vector  the dense right-hand-side vectors
///
/// @return         the dense solution vectors
///
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
auto Solver<Scalar_, outer_uplo_, num_level_, uplo_>::solve( const Matrix_ &vector ) noexcept {

  // ======== Vectors reordering ========================================================================================== //
#ifdef LORASC_PROFILE
std::cout << std::left << std::setw(48) << "Vectors reordering" << std::flush;
auto start = omp_get_wtime();
#endif

  // Reorder vector
  Matrix_ vector_ordered = perm_ * vector;

  // Copy data to subdomains and separator
  for ( auto i = 0; i < num_subdomain_; ++i ) {
    subdomain_[i].vector0() = vector_ordered.middleRows(partition_start_[i], partition_size_[i]);
#ifdef LORASC_PROFILE
std::cout << '.' << std::flush;
#endif
  }
  separator_.vector0() = vector_ordered.bottomRows(partition_size_[num_subdomain_]);
#ifdef LORASC_PROFILE
std::cout << '.' << std::flush;
#endif

#ifdef LORASC_PROFILE
std::cout << '\r' << std::left << std::setw(28) << "Vectors reordering"
          << std::setw(12) << (omp_get_wtime() - start) << " sec" << std::endl;
#endif

  // ======== Solving linear system ======================================================================================= //

  // Forward substitution
#ifdef LORASC_PROFILE
std::cout << std::left << std::setw(48) << "Forward substitution" << std::flush;
start = omp_get_wtime();
#endif

  solveForward();

#ifdef LORASC_PROFILE
std::cout << '\r' << std::left << std::setw(28) << "Forward substitution"
          << std::setw(12) << (omp_get_wtime() - start) << " sec" << std::endl;
#endif

  // Solving separator
#ifdef LORASC_PROFILE
std::cout << std::left << std::setw(48) << "Solving separator" << std::flush;
start = omp_get_wtime();
#endif

  solveSeparator();

#ifdef LORASC_PROFILE
std::cout << '\r' << std::left << std::setw(28) << "Solving separator"
          << std::setw(12) << (omp_get_wtime() - start) << " sec" << std::endl;
#endif

  // Backward substitution
#ifdef LORASC_PROFILE
std::cout << std::left << std::setw(48) << "Backward substitution" << std::flush;
start = omp_get_wtime();
#endif

  solveBackward();

#ifdef LORASC_PROFILE
std::cout << '\r' << std::left << std::setw(28) << "Backward substitution"
          << std::setw(12) << (omp_get_wtime() - start) << " sec" << std::endl;
#endif

  // ======== Reorder solution vectors ==================================================================================== //
#ifdef LORASC_PROFILE
std::cout << std::left << std::setw(48) << "Reorder solution vectors" << std::flush;
start = omp_get_wtime();
#endif

  // Copy data to subdomains and separator
  for ( auto i = 0; i < num_subdomain_; ++i ) {
    vector_ordered.middleRows(partition_start_[i], partition_size_[i]) = subdomain_[i].vector0();
#ifdef LORASC_PROFILE
std::cout << '.' << std::flush;
#endif
  }
  vector_ordered.bottomRows(partition_size_[num_subdomain_]) = separator_.vector1();
#ifdef LORASC_PROFILE
std::cout << '.' << std::flush;
#endif

  // Reorder vector
  Matrix_ solution = iperm_ * vector_ordered;

#ifdef LORASC_PROFILE
std::cout << '\r' << std::left << std::setw(28) << "Reorder solution vectors"
          << std::setw(12) << (omp_get_wtime() - start) << " sec" << std::endl;
#endif

  // ====================================================================================================================== //

#ifdef LORASC_PROFILE
std::cout << std::endl;
std::cout << "max_iterations  = "   << max_iterations_    << std::endl;
std::cout << "tolerance       = "   << tolerance_         << std::endl;
std::cout << "rank_rate       = 1/" << 1/rank_rate_       << std::endl;
std::cout << "trail_rank_rate = 1/" << 1/trail_rank_rate_ << std::endl;
std::cout << "tau             = "   << 1/(1-eigen_solver_.eigenvalues().tail(1).value()) << std::endl;
std::cout << "iterations      = "   << iterative_solver_.iterations() << std::endl;
std::cout << "error           = "   << iterative_solver_.error() << std::endl;

// Eigen::EigenSolver<Matrix_> eigen_solver;
// Vector_ eigs;
// double eigmax, eigmin;
// Matrix_ schur = schur_ * Matrix_::Identity(separator_.size(), separator_.size());
// eigen_solver.compute(separator_.cholesky().solve(schur));
// eigs = eigen_solver.eigenvalues().array().abs();
// std::sort(eigs.data(), eigs.data() + eigs.size(), [](auto a, auto b) { return a > b; });
// eigmax = eigs.head(1).value();
// eigmin = eigs.tail(1).value();
// std::cout << std::endl;
// std::cout << "eigmax_Schur    = "   << eigmax << std::endl;
// std::cout << "eigmin_Schur    = "   << eigmin << std::endl;
// std::cout << "cond_Schur      = "   << eigmax / eigmin << std::endl;
// eigen_solver.compute(iterative_solver_.preconditioner().solve(schur));
// eigs = eigen_solver.eigenvalues().array().abs();
// std::sort(eigs.data(), eigs.data() + eigs.size(), [](auto a, auto b) { return a > b; });
// eigmax = eigs.head(1).value();
// eigmin = eigs.tail(1).value();
// std::cout << std::endl;
// std::cout << "eigmax_Schur    = "   << eigmax << std::endl;
// std::cout << "eigmin_PrecS    = "   << eigmin << std::endl;
// std::cout << "cond_PrecS      = "   << eigmax / eigmin << std::endl;

std::cout << std::endl << "sizes =" << std::endl;
  for ( auto &subdomain : subdomain_ ) {
std::cout << std::setw(8) << subdomain.size();
  }
std::cout << separator_.size() << std::endl << std::endl;
#endif

  return solution;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Solve the linear system - Forward substitution
///
/// @param  vector  the dense right-hand-side vectors
///
/// @note
/// vector_i0 : B_i -> B_i @n
/// vector_s0 : B_s -> Y_s
/// vector_s1 : nan -> nan
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
void Solver<Scalar_, outer_uplo_, num_level_, uplo_>::solveForward() noexcept {
  // Y_s(vector_s0) := B_s(vector_s0) - sum( A_si * inv(A_ii) * B_i(vector_i0) )
  for ( auto &subdomain : subdomain_ ) {
    separator_.vector0().noalias() -= subdomain.interface().transpose() * subdomain.cholesky().solve(subdomain.vector0());
    assert(subdomain.cholesky().info() == Eigen::Success);
#ifdef LORASC_PROFILE
std::cout << '.' << std::flush;
#endif
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Solve the linear system - Separator solving
///
/// @param  vector  the dense right-hand-side vectors
///
/// @note
/// vector_i0 : B_i -> B_i @n
/// vector_s0 : Y_s -> nan
/// vector_s1 : nan -> X_s
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
void Solver<Scalar_, outer_uplo_, num_level_, uplo_>::solveSeparator() noexcept {
  // X_s(vector_s1) := inv(schur) * Y_s(vector_s0)
  separator_.vector1() = iterative_solver_.solve(separator_.vector0());
  warning(iterative_solver_.info() != Eigen::NumericalIssue);
  warning(iterative_solver_.info() != Eigen::NoConvergence);
  assert(iterative_solver_.info()  != Eigen::InvalidInput);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Solve the linear system - Backward substitution
///
/// @param  vector  the dense right-hand-side vectors
///
/// @note
/// vector_i0 : B_i -> X_i @n
/// vector_s0 : nan -> nan
/// vector_s1 : X_s -> X_s
template <typename Scalar_, int outer_uplo_, int num_level_, int uplo_>
void Solver<Scalar_, outer_uplo_, num_level_, uplo_>::solveBackward() noexcept {
  // X_i(vector_i0) := inv(A_ii) * ( B_i(vector_i0) - A_is * X_s(vector_s1) )
  for ( auto &subdomain : subdomain_ ) {
    // D_i(vector_i0) = B_i(vector_i0) - A_is * X_s(vector_s1)
    subdomain.vector0().noalias() -= subdomain.interface() * separator_.vector1();

    // X_i(vector_i0) = inv(A_ii) * D_i(vector_i0)
    subdomain.vector0() = subdomain.cholesky().solve(subdomain.vector0());
    assert(subdomain.cholesky().info() == Eigen::Success);
#ifdef LORASC_PROFILE
std::cout << '.' << std::flush;
#endif
  }
}

}  // namespace lorasc

#endif  // LORASC_CORE_SOLVER_IMPL_HPP_
