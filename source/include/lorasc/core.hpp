////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/core.hpp
/// @brief   The core header
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_CORE_HPP_
#define LORASC_CORE_HPP_

#include "config.hpp"
#include "constant.hpp"
#include "core/solver.hpp"
#include "core/solver_impl.hpp"
#include "core/subdomain.hpp"
#include "core/subdomain_impl.hpp"
#include "core/separator.hpp"
#include "core/separator_impl.hpp"
#include "core/data.hpp"

#endif  // LORASC_CORE_HPP_
