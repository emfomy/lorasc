////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/auxiliary/schur.hpp
/// @brief   The definition of the Schur complement
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_AUXILIARY_SCHUR_HPP_
#define LORASC_AUXILIARY_SCHUR_HPP_

#include <Eigen/Core>
#include "../config.hpp"
#include "../constant.hpp"
#include "../core/data.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace lorasc
//
namespace lorasc {

template <typename Scalar_, int uplo_, int num_subdomain_>                class Schur;
template <typename Scalar_, int uplo_, int num_subdomain_, typename Rhs_> class SchurProductReturn;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// The virtual matrix class of the Schur complement
///
/// @tparam  Scalar_         the scalar type
/// @tparam  uplo_           the triangular storage, can be either @c Lower or @c Upper
/// @tparam  num_subdomain_  the number of subdomains
///
template <typename Scalar_, int uplo_, int num_subdomain_>
class Schur : public Eigen::EigenBase<Schur<Scalar_, uplo_, num_subdomain_>> {

 private:

  /// Type alias
  //@{
  typedef Data<Scalar_, uplo_, num_subdomain_> Data_;

 public:

  typedef Scalar_                                                                 Scalar;
  typedef typename Eigen::NumTraits<Scalar_>::Real                                RealScalar;
  typedef typename Eigen::EigenBase<Schur<Scalar_, uplo_, num_subdomain_>>::Index Index;
  //@}

  enum {
    ColsAtCompileTime    = Eigen::Dynamic,
    RowsAtCompileTime    = Eigen::Dynamic,
    MaxColsAtCompileTime = Eigen::Dynamic,
    MaxRowsAtCompileTime = Eigen::Dynamic,
  };

 protected:

  /// The data
  const Data_ *data_;

 public:

  // Constructors
  Schur();
  Schur( const Data_ &data );

  // Default destructor
  ~Schur();

  // Get sizes
  auto rows() const noexcept;
  auto cols() const noexcept;

  // Get data reference
  const auto& data() const noexcept;

  // Resize the matrix
  void resize( Index nrow, Index ncol ) noexcept;

  // Multiplication operator
  template <typename Rhs_> auto operator*( const Eigen::MatrixBase<Rhs_> &rhs ) const noexcept;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// The proxy class of the matrix product of class Schur
///
/// @tparam  Scalar_         the scalar type
/// @tparam  uplo_           the triangular storage, can be either @c Lower or @c Upper
/// @tparam  num_subdomain_  the number of subdomains
/// @tparam  Rhs_            the right-hand-side matrix type
///
template <typename Scalar_, int uplo_, int num_subdomain_, typename Rhs_>
class SchurProductReturn : public Eigen::ReturnByValue<SchurProductReturn<Scalar_, uplo_, num_subdomain_, Rhs_>> {

 private:

  /// Type alias
  //@{
  typedef Schur<Scalar_, uplo_, num_subdomain_> Schur_;

 public:

  typedef typename Schur_::Index Index;
  //@}

 protected:

  /// The virtual matrix
  const Schur_ &matrix_;

  /// The right-hand-side matrix
  typename Rhs_::Nested &rhs_;

 public:

  // Default constructor
  SchurProductReturn( const Schur_ &matrix, const Rhs_ &rhs );

  // Get sizes
  auto rows() const noexcept;
  auto cols() const noexcept;

  // Evaluate product
  template <typename Dst_> void evalTo( Dst_ &dst ) const noexcept;

};

}  // namespace lorasc

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace Eigen
//
namespace Eigen {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace internal
//
namespace internal {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// The trait struct of class Schur
///
/// @tparam  Scalar_         the scalar type
/// @tparam  uplo_           the triangular storage, can be either @c Lower or @c Upper
/// @tparam  num_subdomain_  the number of subdomains
///
template <typename Scalar_, int uplo_, int num_subdomain_>
struct traits<lorasc::Schur<Scalar_, uplo_, num_subdomain_>> : Eigen::internal::traits<Eigen::SparseMatrix<Scalar_>> {};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// The trait struct of class SchurProductReturn
///
/// @tparam  Scalar_         the scalar type
/// @tparam  uplo_           the triangular storage, can be either @c Lower or @c Upper
/// @tparam  num_subdomain_  the number of subdomains
/// @tparam  Rhs_             the right-hand-side matrix type
///
template <typename Scalar_, int uplo_, int num_subdomain_, typename Rhs_>
struct traits<lorasc::SchurProductReturn<Scalar_, uplo_, num_subdomain_, Rhs_>> {
  /// Type alias
  typedef Eigen::Matrix<typename Rhs_::Scalar, Eigen::Dynamic, Rhs_::ColsAtCompileTime> ReturnType;
};

}  // namespace internal

}  // namespace Eigen

#endif  // LORASC_AUXILIARY_SCHUR_HPP_
