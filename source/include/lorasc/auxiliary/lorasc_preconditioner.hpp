////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/auxiliary/lorasc_preconditioner.hpp
/// @brief   The definition of the lorasc preconditioner
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_AUXILIARY_LORASC_PRECONDITIONER_HPP_
#define LORASC_AUXILIARY_LORASC_PRECONDITIONER_HPP_

#include <Eigen/Core>
#include "../config.hpp"
#include "../constant.hpp"
#include "../core/data.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace lorasc
//
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// The lorasc preconditioner
///
/// @tparam  Scalar_         the scalar type
/// @tparam  uplo_           the triangular storage, can be either @c Lower or @c Upper
/// @tparam  num_subdomain_  the number of subdomains
///
template <typename Scalar_, int uplo_, int num_subdomain_>
class LorascPreconditioner {

 private:

  /// Type alias
  //@{
  typedef Data<Scalar_, uplo_, num_subdomain_>    Data_;

 public:

  typedef Eigen::MatrixX<Scalar_>                 MatrixType;
  typedef typename Eigen::MatrixX<Scalar_>::Index Index;
  //@}

 protected:

  /// The data
  const Data_ *data_;

  /// The temporary matrix
  MatrixType tmp_;

 public:

  // Constructors
  LorascPreconditioner();
  LorascPreconditioner( const Data_ &data );

  // Default destructor
  ~LorascPreconditioner();

  // Get sizes
  auto rows() const noexcept;
  auto cols() const noexcept;

  // Get data reference
  const auto& data() const noexcept;

  // Set data
  void data( const Data_ &data ) noexcept;

  // Analyze pattern
  template <typename Matrix_>
  auto& analyzePattern( const Matrix_ &matrix ) noexcept;

  // Factorize
  template <typename Matrix_>
  auto& factorize( const Matrix_ &matrix ) noexcept;

  // Compute decomposition
  template <typename Matrix_>
  auto& compute( const Matrix_ &matrix ) noexcept;

  // Solve linear system
  template <typename Rhs_>
  inline const auto solve( const Eigen::MatrixBase<Rhs_> &rhs ) const noexcept;

  // Solve linear system
  template <typename Rhs_, typename Dst_> void _solve( const Rhs_ &rhs, Dst_ &dst ) const noexcept;
};

}  // namespace lorasc

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace Eigen
//
namespace Eigen {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace internal
//
namespace internal {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// The return value struct of class LorascPreconditioner
///
/// @tparam  Scalar_         the scalar type
/// @tparam  uplo_           the triangular storage, can be either @c Lower or @c Upper
/// @tparam  num_subdomain_  the number of subdomains
/// @tparam  Rhs_            the right-hand-side matrix type
///
template <typename Scalar_, int uplo_, int num_subdomain_, typename Rhs_>
struct solve_retval<lorasc::LorascPreconditioner<Scalar_, uplo_, num_subdomain_>, Rhs_>
  : solve_retval_base<lorasc::LorascPreconditioner<Scalar_, uplo_, num_subdomain_>, Rhs_> {

  /// Type alias
  typedef lorasc::LorascPreconditioner<Scalar_, uplo_, num_subdomain_> Dec;
  EIGEN_MAKE_SOLVE_HELPERS(Dec, Rhs_)

  // Evaluate solving result
  template <typename Dst_>
  void evalTo( Dst_ &dst ) const noexcept;

};

}  // namespace internal

}  // namespace Eigen

#endif  // LORASC_AUXILIARY_LORASC_PRECONDITIONER_HPP_
