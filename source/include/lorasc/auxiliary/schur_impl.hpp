////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/auxiliary/schur_impl.hpp
/// @brief   The implementation of the Schur complement
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_AUXILIARY_SCHUR_IMPL_HPP_
#define LORASC_AUXILIARY_SCHUR_IMPL_HPP_

#include <type_traits>
#include "schur.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace lorasc
//
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default constructor
///
template <typename Scalar_, int uplo_, int num_subdomain_>
Schur<Scalar_, uplo_, num_subdomain_>::Schur() : data_(nullptr) {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Construct with data
///
/// @param  data  the pointer of data
///
template <typename Scalar_, int uplo_, int num_subdomain_>
Schur<Scalar_, uplo_, num_subdomain_>::Schur( const Data_ &data ) : data_(&data) {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default destructor
///
template <typename Scalar_, int uplo_, int num_subdomain_>
Schur<Scalar_, uplo_, num_subdomain_>::~Schur() {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the number of rows
///
/// @return  the number of rows
///
template <typename Scalar_, int uplo_, int num_subdomain_>
auto Schur<Scalar_, uplo_, num_subdomain_>::rows() const noexcept {
  return data().separator.size();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the number of columns
///
/// @return  the number of columns
///
template <typename Scalar_, int uplo_, int num_subdomain_>
auto Schur<Scalar_, uplo_, num_subdomain_>::cols() const noexcept {
  return data().separator.size();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the data
///
/// @return  a reference of the data
///
template <typename Scalar_, int uplo_, int num_subdomain_>
const auto& Schur<Scalar_, uplo_, num_subdomain_>::data() const noexcept {
  eigen_assert(data_ != nullptr && "lorasc::Schur is not initialized.");
  return *data_;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Resize the matrix
///
/// @param  nrow  the number of rows
/// @param  ncol  the number of columns
///
template <typename Scalar_, int uplo_, int num_subdomain_>
void Schur<Scalar_, uplo_, num_subdomain_>::resize( Index nrow, Index ncol ) noexcept {
  static_cast<void>(nrow);
  static_cast<void>(ncol);
  eigen_assert((nrow == 0 && ncol == 0) || (nrow == rows() && ncol == cols()));
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Multiplication operator, the matrix-matrix multiplication
///
/// @tparam  Rhs_  the right-hand-side matrix type
///
/// @param   rhs   the right-hand-side matrix
///
template <typename Scalar_, int uplo_, int num_subdomain_> template <typename Rhs_>
auto Schur<Scalar_, uplo_, num_subdomain_>::operator*( const Eigen::MatrixBase<Rhs_> &rhs ) const noexcept {
  return SchurProductReturn<Scalar_, uplo_, num_subdomain_, Rhs_>(*this, rhs.derived());
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default constructor
///
/// @param   matrix  the virtual matrix
/// @param   rhs     the right-hand-side matrix
///
template <typename Scalar_, int uplo_, int num_subdomain_, typename Rhs_>
SchurProductReturn<Scalar_, uplo_, num_subdomain_, Rhs_>::SchurProductReturn(
    const Schur_ &matrix,
    const Rhs_ &rhs
) : matrix_(matrix),
    rhs_(rhs) {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the number of rows
///
/// @return  the number of rows
///
template <typename Scalar_, int uplo_, int num_subdomain_, typename Rhs_>
auto SchurProductReturn<Scalar_, uplo_, num_subdomain_, Rhs_>::rows() const noexcept {
  return matrix_.rows();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the number of columns
///
/// @return  the number of columns
///
template <typename Scalar_, int uplo_, int num_subdomain_, typename Rhs_>
auto SchurProductReturn<Scalar_, uplo_, num_subdomain_, Rhs_>::cols() const noexcept {
  return rhs_.cols();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Evaluate the matrix-matrix multiplication
///
/// @tparam  Dst_  the product matrix type
///
/// @param   dst   the product matrix
///
template <typename Scalar_, int uplo_, int num_subdomain_, typename Rhs_> template <typename Dst_>
void SchurProductReturn<Scalar_, uplo_, num_subdomain_, Rhs_>::evalTo( Dst_ &dst ) const noexcept {
  // dst := A_ss * rhs - sum( A_si * inv(A_ii) * A_is * rhs )
  dst.noalias() = matrix_.data().separator.separator_view() * rhs_;
  for ( auto &subdomain : matrix_.data().subdomain ) {
    dst.noalias() -= subdomain.interface().transpose() * subdomain.cholesky().solve(subdomain.interface() * rhs_);
  }
}

}  // namespace lorasc

#endif  // LORASC_AUXILIARY_SCHUR_IMPL_HPP_
