////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/auxiliary/approximated_selfadjoint_eigen_solver_randomized_svd.hpp
/// @brief   The definition of the approximated self-adjoint eigenvalue solver using randomized algorithm
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_AUXILIARY_APPROXIMATED_SELFADJOINT_EIGEN_SOLVER_RANDOMIZED_SVD_HPP_
#define LORASC_AUXILIARY_APPROXIMATED_SELFADJOINT_EIGEN_SOLVER_RANDOMIZED_SVD_HPP_

#include <Eigen/Core>
#include "../config.hpp"
#include "../constant.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace lorasc
//
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// The LORASC solver class
///
/// @tparam  MatrixType_ the matrix type
///
template <typename MatrixType_>
class ApproximatedSelfadjointEigenSolverRandomizedSVD {

 private:

  /// Type alias
  //@{
  typedef typename MatrixType_::Scalar Scalar_;
  typedef Eigen::MatrixX<Scalar_>      Matrix_;
  typedef Eigen::VectorX<Scalar_>      Vector_;
  typedef Eigen::JacobiSVD<Matrix_>    SVDSolver_;
  //@}

  /// The rank
  index_t rank_;

  /// The rate of the rank
  Scalar_ rank_rate_;

  /// The rate of the trailing rank
  Scalar_ trail_rank_rate_;

 protected:

  /// The SVD solver
  SVDSolver_ svd_solver_;

 public:

  // Default Constructor
  ApproximatedSelfadjointEigenSolverRandomizedSVD();

  // Default Destructor
  ~ApproximatedSelfadjointEigenSolverRandomizedSVD();

  // Set rate of rank
  auto& setRankRate( const Scalar_ rank_rate ) noexcept;

  // Set rate of trailing rank
  auto& setTrailRankRate( const Scalar_ trail_rank_rate ) noexcept;

  // Get eigenvectors
  inline auto eigenvectors() const noexcept;

  // Get eigenvalues
  inline auto eigenvalues() const noexcept;

  // Compute decomposition
  auto& compute( const MatrixType_ &matrix ) noexcept;

};

}  // namespace lorasc

#endif  // LORASC_AUXILIARY_APPROXIMATED_SELFADJOINT_EIGEN_SOLVER_RANDOMIZED_SVD_HPP_
