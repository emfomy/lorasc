////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/auxiliary/approximated_selfadjoint_eigen_solver_randomized_svd_impl.hpp
/// @brief   The implementation of the approximated self-adjoint eigenvalue solver using randomized algorithm
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_AUXILIARY_APPROXIMATED_SELFADJOINT_EIGEN_SOLVER_RANDOMIZED_SVD_IMPL_HPP_
#define LORASC_AUXILIARY_APPROXIMATED_SELFADJOINT_EIGEN_SOLVER_RANDOMIZED_SVD_IMPL_HPP_

#include "approximated_selfadjoint_eigen_solver_randomized_svd.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace lorasc
//
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default constructor
///
template <typename MatrixType_>
ApproximatedSelfadjointEigenSolverRandomizedSVD<MatrixType_>::ApproximatedSelfadjointEigenSolverRandomizedSVD()
  : rank_(0),
    rank_rate_(kRankRate),
    trail_rank_rate_(kTrailRankRate) {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default destructor
///
template <typename MatrixType_>
ApproximatedSelfadjointEigenSolverRandomizedSVD<MatrixType_>::~ApproximatedSelfadjointEigenSolverRandomizedSVD() {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the rate of the rank
///
/// @param  rank_rate  the rate of the rank
///
/// @return            this solver
///
template <typename MatrixType_>
auto& ApproximatedSelfadjointEigenSolverRandomizedSVD<MatrixType_>::setRankRate( const Scalar_ rank_rate ) noexcept {
  rank_rate_ = rank_rate;
  return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the rate of the trailing rank
///
/// @param  trail_rank_rate  the rate of the trailing rank
///
/// @return                  this solver
///
template <typename MatrixType_>
auto& ApproximatedSelfadjointEigenSolverRandomizedSVD<MatrixType_>::setTrailRankRate( const Scalar_ trail_rank_rate ) noexcept {
  trail_rank_rate_ = trail_rank_rate;
  return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the eigenvectors
///
/// @return  a reference of the eigenvectors
///
template <typename MatrixType_>
inline auto ApproximatedSelfadjointEigenSolverRandomizedSVD<MatrixType_>::eigenvectors() const noexcept {
  eigen_assert(rank_ > 0 && "lorasc::ApproximatedSelfadjointEigenSolverRandomizedSVD is not initialized.");
  return svd_solver_.matrixU().leftCols(rank_);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the eigenvalues
///
/// @return  a reference of the eigenvalues
///
template <typename MatrixType_>
inline auto ApproximatedSelfadjointEigenSolverRandomizedSVD<MatrixType_>::eigenvalues() const noexcept {
  eigen_assert(rank_ > 0 && "lorasc::ApproximatedSelfadjointEigenSolverRandomizedSVD is not initialized.");
  return svd_solver_.singularValues().head(rank_);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Compute the decomposition of the matrix
///
/// @param   matrix   the matrix
///
/// @return           the decomposed solver
///
template <typename MatrixType_>
auto& ApproximatedSelfadjointEigenSolverRandomizedSVD<MatrixType_>::compute( const MatrixType_ &matrix ) noexcept {

  assert(rank_rate_ >= 0                    && "The rank must be positive.");
  assert(trail_rank_rate_ >= 0              && "The trailing rank must be non-negative.");
  assert(rank_rate_ + trail_rank_rate_ <= 1 && "The total rank must be less than 1.");

  const index_t size       = matrix.cols();
  rank_                    = std::max(static_cast<index_t>(size * rank_rate_), std::min(size, 16));
  const index_t total_rank = rank_ + size * trail_rank_rate_;

  // Randomized algorithm
  Matrix_ tmp = Matrix_::Random(size, total_rank);
  for ( auto i = 0; i < rank_; ++i ) {
#pragma warning
    tmp = matrix * tmp;
#ifdef LORASC_PROFILE
std::cout << '.' << std::flush;
#endif
  }
  tmp.noalias() = matrix * svd_solver_.compute(tmp, Eigen::ComputeThinU).matrixU();
#ifdef LORASC_PROFILE
std::cout << '.' << std::flush;
#endif
  svd_solver_.compute(tmp, Eigen::ComputeThinU);
#ifdef LORASC_PROFILE
std::cout << '.' << std::flush;
#endif

  return *this;
}

}  // namespace lorasc

#endif  // LORASC_AUXILIARY_APPROXIMATED_SELFADJOINT_EIGEN_SOLVER_RANDOMIZED_SVD_IMPL_HPP_
