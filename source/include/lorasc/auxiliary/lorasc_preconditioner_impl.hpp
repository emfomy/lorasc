////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/auxiliary/lorasc_preconditioner_impl.hpp
/// @brief   The implementation of the lorasc preconditioner
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORLORASC_AUXILIARY_LORASC_PRECONDITIONER_IMPL_HPP_
#define LORLORASC_AUXILIARY_LORASC_PRECONDITIONER_IMPL_HPP_

#include "lorasc_preconditioner.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace lorasc
//
namespace lorasc {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default constructor
///
template <typename Scalar_, int uplo_, int num_subdomain_>
LorascPreconditioner<Scalar_, uplo_, num_subdomain_>::LorascPreconditioner() : data_(nullptr) {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Construct with data
///
/// @param  data  the pointer of data
///
template <typename Scalar_, int uplo_, int num_subdomain_>
LorascPreconditioner<Scalar_, uplo_, num_subdomain_>::LorascPreconditioner( const Data_ &data ) : data_(&data) {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Default destructor
///
template <typename Scalar_, int uplo_, int num_subdomain_>
LorascPreconditioner<Scalar_, uplo_, num_subdomain_>::~LorascPreconditioner() {}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the number of rows
///
/// @return  the number of rows
///
template <typename Scalar_, int uplo_, int num_subdomain_>
auto LorascPreconditioner<Scalar_, uplo_, num_subdomain_>::rows() const noexcept {
  return data().separator.size();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the number of columns
///
/// @return  the number of columns
///
template <typename Scalar_, int uplo_, int num_subdomain_>
auto LorascPreconditioner<Scalar_, uplo_, num_subdomain_>::cols() const noexcept {
  return data().separator.size();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Get a reference of the data
///
/// @return  a reference of the data
///
template <typename Scalar_, int uplo_, int num_subdomain_>
const auto& LorascPreconditioner<Scalar_, uplo_, num_subdomain_>::data() const noexcept {
  eigen_assert(data_ != nullptr && "lorasc::LorascPreconditioner is not initialized.");
  return *data_;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Set data
///
/// @param  data  the pointer of data
///
template <typename Scalar_, int uplo_, int num_subdomain_>
void LorascPreconditioner<Scalar_, uplo_, num_subdomain_>::data( const Data_ &data ) noexcept {
  data_ = &data;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Analyze the pattern of the matrix
///
/// @tparam  MatrixType_  the matrix type
///
/// @param  matrix        the matrix
///
/// @return               the analyzed preconditioner
///
/// @note                 Do nothing for LorascPreconditioner
///
template <typename Scalar_, int uplo_, int num_subdomain_> template <typename MatrixType_>
auto& LorascPreconditioner<Scalar_, uplo_, num_subdomain_>::analyzePattern( const MatrixType_ &matrix ) noexcept {
  static_cast<void>(matrix);
  return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Factorize the matrix
///
/// @tparam  MatrixType_  the matrix type
///
/// @param  matrix        the matrix
///
/// @return               the factorized preconditioner
///
/// @note                 Do nothing for LorascPreconditioner
///
template <typename Scalar_, int uplo_, int num_subdomain_> template <typename MatrixType_>
auto& LorascPreconditioner<Scalar_, uplo_, num_subdomain_>::factorize( const MatrixType_ &matrix ) noexcept {
  static_cast<void>(matrix);
  return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Compute the decomposition of the matrix
///
/// @tparam  MatrixType_  the matrix type
///
/// @param   matrix       the matrix
///
/// @return               the computed preconditioner
///
/// @note                 Do nothing for LorascPreconditioner
///
template <typename Scalar_, int uplo_, int num_subdomain_> template <typename MatrixType_>
auto& LorascPreconditioner<Scalar_, uplo_, num_subdomain_>::compute( const MatrixType_ &matrix ) noexcept {
  static_cast<void>(matrix);
  return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Solve the linear system
///
/// @tparam  Rhs_  the right-hand-side matrix type
///
/// @param   rhs   the right-hand-side matrix
///
template <typename Scalar_, int uplo_, int num_subdomain_> template <typename Rhs_>
inline const auto LorascPreconditioner<Scalar_, uplo_, num_subdomain_>::solve(
    const Eigen::MatrixBase<Rhs_> &rhs
) const noexcept {
  eigen_assert(data_ != nullptr && "lorasc::LorascPreconditioner is not initialized.");
  eigen_assert(cols() == rhs.rows() && "Invalid number of rows of the right-hand-side matrix");
  return Eigen::internal::solve_retval<LorascPreconditioner<Scalar_, uplo_, num_subdomain_>, Rhs_>(*this, rhs.derived());
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Solve the linear system
///
/// @tparam  Rhs_  the right-hand-side matrix type
/// @tparam  Dst_  the solving result type
///
/// @param   rhs   the right-hand-side matrix
/// @param   dst   the solving result
///
template <typename Scalar_, int uplo_, int num_subdomain_> template <typename Rhs_, typename Dst_>
void LorascPreconditioner<Scalar_, uplo_, num_subdomain_>::_solve( const Rhs_ &rhs, Dst_ &dst ) const noexcept {
  dst.noalias()  = data().separator.eigenvectors() *
                  (data().separator.eigenvalues().asDiagonal() *
                  (data().separator.eigenvectors().transpose() * rhs));
  dst.noalias() += data().separator.cholesky().solve(rhs);
}

}  // namespace lorasc

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace Eigen
//
namespace Eigen {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace internal
//
namespace internal {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Evaluate the solving result
///
/// @tparam  Dst_  the solving result type
///
/// @param   dst   the solving result
///
template <typename Scalar_, int uplo_, int num_subdomain_, typename Rhs_> template <typename Dst_>
void solve_retval<lorasc::LorascPreconditioner<Scalar_, uplo_, num_subdomain_>, Rhs_>::evalTo( Dst_& dst ) const noexcept {
  dec()._solve(rhs(), dst);
}

}  // namespace internal

}  // namespace Eigen

#endif  // LORASC_AUXILIARY_LORASC_PRECONDITIONER_IMPL_HPP_
