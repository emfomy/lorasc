////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/constant.hpp
/// @brief   The constants and definitions
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_CONSTANT_HPP_
#define LORASC_CONSTANT_HPP_

#include <cassert>
#include <Eigen/Dense>
#include <Eigen/Sparse>

#ifndef DOXYGEN_SHOULD_SKIP_THIS

#ifndef NDEBUG
  #define warning(EX) (void)((EX) || fprintf(stderr, "%s:%u: Warning `%s' failed.\n", __FILE__, __LINE__, #EX))
#else
  #define warning(EX)
#endif

#endif  // DOXYGEN_SHOULD_SKIP_THIS

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace Eigen
//
namespace Eigen {

/// Type alias
//@{

// Sparse matrix alias
typedef SparseMatrix<std::complex<double>> SpMatrixXcd;
typedef SparseMatrix<std::complex<float>>  SpMatrixXcf;
typedef SparseMatrix<double>               SpMatrixXd;
typedef SparseMatrix<float>                SpMatrixXf;
typedef SparseMatrix<int>                  SpMatrixXi;

// Sparse vector alias
typedef SparseVector<std::complex<double>> SpVectorXcd;
typedef SparseVector<std::complex<float>>  SpVectorXcf;
typedef SparseVector<double>               SpVectorXd;
typedef SparseVector<float>                SpVectorXf;
typedef SparseVector<int>                  SpVectorXi;

// Matrix alias
template <typename Scalar_> using MatrixX    = Matrix<Scalar_, Dynamic, Dynamic>;
template <typename Scalar_> using VectorX    = Matrix<Scalar_, Dynamic, 1>;
template <typename Scalar_> using RowVectorX = Matrix<Scalar_, 1, Dynamic>;
template <typename Scalar_> using SpMatrixX  = SparseMatrix<Scalar_>;
template <typename Scalar_> using SpVectorX  = SparseVector<Scalar_>;

//@}

}  // namespace Eigen

#endif  // LORASC_CONSTANT_HPP_
