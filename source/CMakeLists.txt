# The CMake setting of 'source/'

# Create configure files
file(
  GLOB_RECURSE FILES
  RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}/config" "${CMAKE_CURRENT_SOURCE_DIR}/config/*"
)
foreach(FILE IN LISTS FILES)
  configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/config/${FILE}"
    "${CMAKE_CURRENT_BINARY_DIR}/../${FILE}"
  )
endforeach()

# Create direct links of include files
file(
  GLOB_RECURSE FILES
  RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}/include/*"
)
foreach(FILE IN LISTS FILES)
  get_filename_component(DIR "${CMAKE_CURRENT_BINARY_DIR}/../${FILE}" DIRECTORY)
  file(MAKE_DIRECTORY ${DIR})
  execute_process(
    COMMAND ln -f "${CMAKE_CURRENT_SOURCE_DIR}/${FILE}" "${CMAKE_CURRENT_BINARY_DIR}/../${FILE}"
  )
endforeach()
