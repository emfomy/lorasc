////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    lorasc/config.hpp
/// @brief   The configured options and settings
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#ifndef LORASC_CONFIG_HPP_
#define LORASC_CONFIG_HPP_

#ifndef __cplusplus
#error C++ is required.
#elif __cplusplus < 201402L
#warning C++14 is required.
#endif

#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define LORASC_VERSION_MAJOR @LORASC_VERSION_MAJOR@
#define LORASC_VERSION_MINOR @LORASC_VERSION_MINOR@
#define LORASC_VERSION_PATCH @LORASC_VERSION_PATCH@
#define LORASC_VERSION_MAJOR_STRING "@LORASC_VERSION_MAJOR@"
#define LORASC_VERSION_MINOR_STRING "@LORASC_VERSION_MINOR@"
#define LORASC_VERSION_PATCH_STRING "@LORASC_VERSION_PATCH@"
#endif  // DOXYGEN_SHOULD_SKIP_THIS

#include <cstddef>
#include <Eigen/Core>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The namespace LORASC
//
namespace lorasc {

/// Define the type of matrix index
typedef int index_t;

/// Define the type of size
typedef ptrdiff_t size_t;

/// The default number of partition level
const auto kNumLevel = 4;

/// The default triangular storage
const auto kUplo = Eigen::Upper;

/// The default maximum number of iterations used by iterative solver
const auto kMaxIterations = 256;

/// The default tolerance threshold used by iterative solver
const auto kTolerance = 1e-8;

/// The default rate of the rank used by randomized algorithm
const auto kRankRate = 1.0/256;

/// The default rate of the trailing rank used by randomized algorithm
const auto kTrailRankRate = kRankRate;

}  // namespace lorasc

#endif  // LORASC_CONFIG_HPP_
