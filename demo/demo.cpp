////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file    demo/main.cpp
/// @brief   The demo code
///
/// @author  Mu Yang <emfomy@gmail.com>
///

#include <iostream>
#include <iomanip>
#include <omp.h>
#include <lorasc.hpp>
#include <Eigen/PardisoSupport>
#include <unsupported/Eigen/SparseExtra>

using namespace std;
using namespace Eigen;
using namespace lorasc;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Test the solver
///
template <typename Solver>
void test( Solver &solver, const SpMatrixXd &mat, const VectorXd &rhs ) {
  // Solve linear system
  auto start = omp_get_wtime();
  auto sol = solver.compute(mat).solve(rhs);
#ifdef LORASC_PROFILE
  cout << "Used " << (omp_get_wtime() - start) << " sec." << endl;
#else
  cout << setw(12) << (omp_get_wtime() - start) << '\t' << flush;
#endif


  // Compute relative residual
  auto res = rhs - mat.selfadjointView<Lower>()*sol;
#ifdef LORASC_PROFILE
  cout << "relative residual: " << res.norm() << endl;
#else
  cout << setw(8) << res.norm() << '\t' << flush;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Main function
///
int main( int argc, char *argv[] ) {

#ifdef LORASC_PROFILE
  cout << "LORASC "
       << LORASC_VERSION_MAJOR_STRING << "."
       << LORASC_VERSION_MINOR_STRING << "."
       << LORASC_VERSION_PATCH_STRING << " demo" << endl;
#endif

  if ( argc <= 1 ) {
    cout << "Usage: " << argv[0] << " <mtx-file>" << endl;
    exit(1);
  }

  auto str = strrchr(argv[1], '/') + 1;
  cout << left << setw(24) << str << '\t' << flush;

  // Make Eigen run in parallel
  initParallel();

  // Set cout flags
  cout.setf(ios::boolalpha);

  // Load matrix from file
#ifndef LORASC_PROFILE
  streambuf *old_buf = cout.rdbuf(0);
#endif
  SpMatrixXd mat;
  loadMarket(mat, argv[1]);
#ifndef LORASC_PROFILE
  cout.rdbuf(old_buf);
#endif

  // Create right-hand-side vectors
  VectorXd rhs = VectorXd::Random(mat.cols());
  rhs.normalize();

  {
#ifdef LORASC_PROFILE
    cout << endl << "Solve with LORASC" << endl;
#endif
    Solver<double, Lower> lorasc_solver;
    test(lorasc_solver, mat, rhs);
  }

  {
#ifdef LORASC_PROFILE
    cout << endl << "Solve with Pardiso LLT" << endl;
#endif
    PardisoLLT<SpMatrixXd, Lower> pardiso_solver;
    test(pardiso_solver, mat, rhs);
  }

  {
#ifdef LORASC_PROFILE
    cout << endl << "Solve with Eigen LLT" << endl;
#endif
    SimplicialLLT<SpMatrixXd, Lower> llt_solver;
    test(llt_solver, mat, rhs);
  }

  {
#ifdef LORASC_PROFILE
    cout << endl << "Solve with Eigen CG" << endl;
#endif
    ConjugateGradient<SpMatrixXd, Lower> cg_solver;
    cg_solver.setMaxIterations(65536).setTolerance(kTolerance);
    test(cg_solver, mat, rhs);
#ifdef LORASC_PROFILE
    cout << "iterations = " << cg_solver.iterations() << endl;
    cout << "error      = " << cg_solver.error() << endl;
#endif
  }

  cout << endl;
}
