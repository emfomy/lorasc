# Robust Algebraic Schur Complement Preconditioners Based on Low Rank Corrections

### Git
* https://bitbucket.org/emfomy/lorasc

### Author
* Mu Yang <emfomy@gmail.com>

## Programming

### Compiler
* [GCC 5.1.0](https://gcc.gnu.org/gcc-5/)

### Library
* [Intel® Math Kernel Library 11.3](https://software.intel.com/en-us/intel-mkl)
* [Eigen 3.2.7](http://eigen.tuxfamily.org)
* [METIS 5.1.0](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview)

## Algorithm

### Creating LORASC Preconditioner

#### Reordering
Reorder the **symmetric positive definite** matrix to
```
    [                    ]
    [ A_11          A_11 ]
A = [       %        %   ]
    [          A_nn A_nς ]
    [ A_ς1  %  A_ςn A_ςς ]
    [                    ]
```

#### Factorization
The LDU-factorization is
```
    [                  ] [            ] [                               ]
    [ A_11             ] [ I          ] [ A_11           inv(A_11)*A_1ς ]
A = [       %          ] [    %       ] [       %               %       ]
    [          A_nn    ] [       I    ] [          A_nn  inv(A_nn)*A_nς ]
    [ A_ς1  %  A_ςn  I ] [          S ] [                       I       ]
    [                  ] [            ] [                               ]
```
where *S = A_ςς - sum(A_ςi inv(A_ii) A_is)* is the Schur complement.

#### Generalized Eigenvalues Problem
Let *λ_i* be the smallest *l* generalized eigenvalues of *S u = λ A_ςς u* and *v_i* be the corresponding generalized eigenvectors. Define *σ_i = (ε-λ_i)/λ_i*, where *ε = max(λ_i)*. Note that *ε ≤ κ(inv(Σ)S) ≤ 1*.

#### Approximate Inverse of the Schur Complement
The approximate inverse of the Schur complement is *inv(Σ) = inv(A_ςς) + VDV'*, where *V = [v_1, ..., V_l]* and *D = diag(σ_1, ..., σ_l)*.

### Randomized Algorithm
1. Input: a *m×m* symmetric positive definite matrix *B*, and the desired rank *l*.
2. Sample an *m×l* test matrix *Q* with independent zero-mean, unit-variance Gaussian entries.
3. Let *H ← BQ*.
4. Compute the QR-decomposition *H = QR*.
5. Repeat 2-3 for some iterations.
6. Let *C = BQ*.
7. Compute the SVD-decomposition *C = VDU'*
8. Output: the largest eigenvalues *D*, and the corresponding eigenvectors *V*.

#### Trick
Rewrite generalized eigenvalues problem as *A_ςς u - S u = A_ςς u - λ A_ςς u*, which is equivalent to *inv(A_ςς)(A_ςς - S)u = ζu*, where *ζ = 1 - λ*. Since *0 ≤ λ < 1*, we have *0 < ζ ≤ 1* and the smallest eigenvalues *λ_l* correspond to the largest eigenvalues *ζ_l*.

Write *A_ςς = LL'* be the Cholresky decomposition, then *inv(L)(A_ςς - S)inv(L')w = ζw*, where *w = L'u*. Since *inv(L)(A_ςς - S)inv(L')* is symmetric positive definite, we may use iterative solver (Randomized Algorithm) to solve the generalized eigenvalues problem.

### Solving Linear System
Solve *Ax = b*. Rewrite the system as *A_L A_D A_U x = b*.

#### Forward Substitution
Solve *A_L y = b*:
```
[                                      ] [     ]   [     ]
[       I                              ] [ y_1 ]   [ b_1 ]
[                 %                    ] [  %  ] = [  %  ]
[                          I           ] [ y_n ]   [ b_n ]
[ A_ς1*inv(A_11)  %  A_ςn*inv(A_nn)  I ] [ y_ς ]   [ b_ς ]
[                                      ] [     ]   [     ]
```
1. *y_i = b_i*.
2. Compute *y_ς = b_ς - sum(A_ςi inv(A_ii) y_i)*.

#### Solving Separator
Solve *A_D z = b*:
```
[            ] [     ] = [     ]
[ I          ] [ z_1 ] = [ y_1 ]
[    %       ] [  %  ] = [  %  ]
[       I    ] [ z_n ] = [ y_n ]
[          S ] [ z_ς ] = [ y_ς ]
[            ] [     ] = [     ]
```
1. *z_i = y_i*.
2. Solve *z_ς* from *S z_ς = y_ς* by PCG using *inv(Σ)* as preconditioner.

#### Backward Substitution
Solve *A_U x = z*:
```
[                     ] [     ] = [     ]
[ A_11           A_1ς ] [ x_1 ] = [ z_1 ]
[       %         %   ] [  %  ] = [  %  ]
[          A_nn  A_nς ] [ x_n ] = [ z_n ]
[                 I   ] [ x_ς ] = [ z_ς ]
[                     ] [     ] = [     ]
```
1. *x_ς = z_ς*.
2. Solve *x_i* from *A_ii x_i = z_i - A_is x_ς*.

## Reference
* [Grigori, L., Nataf, F., & Yousef, S. (2014, July 1). Robust Algebraic Schur Complement Preconditioners Based on Low Rank Corrections. Retrieved April 7, 2015](https://hal.inria.fr/hal-01017448/document)
* [Witten, R., & Candès, E. (2014). Randomized Algorithms for Low-Rank Matrix Factorizations: Sharp Performance Bounds. Algorithmica, 72(1), 264–281.](http://doi.org/10.1007/s00453-014-9891-7)
